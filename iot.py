#!/usr/bin/python3
from telegram.ext import Updater, CommandHandler, Job, MessageHandler, Filters
import pickle,re,os,telegram,time,pickle
import os
from telnet import *
from response import *
#global command_status
print("chatbot completed")
from gtts import gTTS
import speech_recognition as sr
r = sr.Recognizer()
#from chatterbot import ChatBot
#chatterbot=ChatBot('Iot-Bot')
udb=None
# Function to conect to the users database 
def connectdb():
    try:
        global udb
        if(os.path.exists("users.db")):
            udb=open("users.db","rb+")
        else:
            udb=open ("users.db","wb+")
        return 0
    except:
        udb=None
        return -1
# Function to read and append to database
def checkdb(bot, update):
    id=update.message.from_user.id
    name=update.message.from_user.name
    if(connectdb()==-1):
        print("Data base error occured!")
        update.message.reply_text("Problem detected, please contact ADMIN @ABHIRAMSHIBU")
    else:
        try:
            udb_dict=pickle.load(udb)
            udb.seek(0)
        except:
            udb_dict={}
            print("Untouched user database detected!")
            try:
                pickle.dump(udb_dict,udb)
                udb.seek(0)
            except:
                print("Picke is picking this code")
        if( id  in udb_dict.keys()):
            name_db=udb_dict[id]
            print("Found in database:",name_db)
        else:
            udb_dict[id]=name.strip("@")
            name_db=udb_dict[id]
            print("User not in database, creating...")
            print(udb_dict.keys())
            try:
                udb.seek(0)
                pickle.dump(udb_dict,udb)
                udb.seek(0)
                print("Wrote data base",name_db)
            except:
                print("Picke is picking this code")
    result=str(name_db)
    return (result,id)
def iotoff(bot,update):
       f=open("/sys/class/gpio/gpio14/value",'w')
       f.write("0")
       f.close()
def ioton(bot,update):
       f=open("/sys/class/gpio/gpio14/value",'w')
       f.write("1")
       f.close()

def iot(bot,update):
       message=update.message.text
       print(update.message.from_user.username+":"+update.message.text)
       list=message.strip("/iot").strip().split()
       try:
           pin=int(list[0])
           flag=1
       except:
          flag=0
       if flag==1:
           if "on" in list:
             if not os.path.exists("/sys/class/gpio/gpio"+list[0]):
                f=open("/sys/class/gpio/export","w")
                f.write(list[0])
                f.close()
             f=open("/sys/class/gpio/gpio"+list[0]+"/direction","w")
             f.write("out")
             f.close()
             f=open("/sys/class/gpio/gpio"+list[0]+"/value",'w')
             f.write("1")
             f.close()
           elif "off" in list:
             if not os.path.exists("/sys/class/gpio/gpio"+list[0]):
                f=open("/sys/class/gpio/export","w")
                f.write(list[0])
                f.close()
             f=open("/sys/class/gpio/gpio"+list[0]+"/direction","w")
             f.write("out")
             f.close()
             f=open("/sys/class/gpio/gpio"+list[0]+"/value",'w')
             f.write("0")
             f.close()
           else:
             update.message.reply_text("Command not found")

       else:
             update.message.reply_text("Pin not found")
def status(bot,update):
     message=update.message.text
     print("Status :"+message)
     print(update.message.from_user.username+":"+update.message.text)
     list=message.strip("/status").strip()
     try:
         pin=int(list)
         flag=1
     except:
         update.message.reply_text("Invalid Pin")
         flag=0
     if(flag==1):
         if(os.path.exists("/sys/class/gpio/gpio"+list)):
               f=open("/sys/class/gpio/gpio"+list+"/value","r")
               update.message.reply_text("Pin status:"+f.read())
               f.close()
         else:
               update.message.reply_text("Pin not in use")
def mode(bot, update):
         try:
               command_status
         except:
                 try:
                      command_status="off"
                 except:
                     command_status="on"
         command_status="on"
         print("Command_status is undeclared so defaulting to on")
         message=update.message.text.strip("/mode").strip()
         print("Mode :"+message)
         print("Check")
         print("Current mode is set to:"+str(command_status))
         print(update.message.from_user.username+":"+update.message.text)
         if(message.lower()=="status"):       #this fails because of programming error
                   print("In ")
                   if(command_status=="on"):
                            update.message.reply_text("Command mode : ON")
                   else:
                            update.message.reply_text("Command mode : OFF")
         elif(message.lower()=="on"):
                  if(command_status=="on"):
                            update.message.reply_text("Command mode already ON!")
                  else:
                            command_status="on"
                            update.message.reply_text("Command mode set to ON")
         elif(message.lower()=="off"):
                  if(command_status=="on"):
                            command_status="off"
                            update.message.reply_text("Command mode set to OFF")
                  else:
                            command_status="off"
                            update.message.reply_text("Command mode alredy OFF")
# Main Message Handler
def message(bot,update):
    try:
         command_status # Declaration of command handler vaiable
    except:
         command_status="off" # Defaulting command handler to off
         #command_status="on"
    if(command_status=="on"): # Command handler set to on then do
       try:
            try:
                os.remove("voice.wav") # Remove previously recived voice
            except:
                print("File dont exist!")  # Handle exception if file is already deleted
            file_id = update.message.voice.file_id
            print("File id:"+file_id)
            newFile = bot.get_file(file_id) # get file from telegram servers
            print("Voice message detected.")
            newFile.download('voice.ogg')  # download file
            os.system("ffmpeg -i voice.ogg voice.wav 2>null") # convert to wav from ogg 
            with sr.AudioFile('voice.wav') as source: # use voice.wav instad of mic
                  print("Converting audio file.")
                  audio =r.record(source)     # recording to google format
            try:
               message=r.recognize_google(audio)   # speech recognize from recorded audio
               print("Recognized message is: "+message)
               update.message.reply_text(str(message))   # Feedback to user
               flag=1    # Set that voice reognition has passed without any failure
            except:
               print("Google recognize failure!, fallback.") # print error message
               flag=0 # set voice not recognized
       except:  # if its not a voice message will raise an error getting file info
            message=update.message.text  # set message from user directly
            print(update.message.from_user.username+":"+update.message.text) # Debug
            flag=1   # Set recognized and ready to use
            print("Text message detected.") # debug

       if(flag==1): # Do only if either voice reognition or text message passed
           pin=open("pin").read().split("\n") # find and get pin
           mess_words=message.split() 
           s="" #status
           p="" #pin numbers
           flagpin=0
           flagstatus=0
           status=["on","off"] 
           print("before loop\n")
           for i in mess_words:
               if i.lower() in status and flagstatus==0:
                   s=i.lower()
                   flagstatus=1
               if i in pin and flagpin==0:
                   p=i
                   flagpin=1
           message=p+" "+s
           print("message value after loop is "+message)
           list=message.strip().split()
           try:
               pin=int(list[0])
               flag=1
           except:
               flag=0
           if flag==1:
               if "on" in list:
                 if not os.path.exists("/sys/class/gpio/gpio"+list[0]):
                    f=open("/sys/class/gpio/export","w")
                    f.write(list[0])
                    f.close()
                 f=open("/sys/class/gpio/gpio"+list[0]+"/direction","w")
                 f.write("out")
                 f.close()
                 f=open("/sys/class/gpio/gpio"+list[0]+"/value",'w')
                 f.write("1")
                 f.close()
                 update.message.reply_text("Pin "+str(list[0])+" changed to on")
               elif "off" in list:
                 if not os.path.exists("/sys/class/gpio/gpio"+list[0]):
                    f=open("/sys/class/gpio/export","w")
                    f.write(list[0])
                    f.close()
                 f=open("/sys/class/gpio/gpio"+list[0]+"/direction","w")
                 f.write("out")
                 f.close()
                 f=open("/sys/class/gpio/gpio"+list[0]+"/value",'w')
                 f.write("0")
                 f.close()
                 update.message.reply_text("Pin "+str(list[0])+" changed to "+list[1])
               else:
                 update.message.reply_text("Command not found")

           else:
                 update.message.reply_text("Pin not found")
       else:
             update.message.reply_text("Speech to text engine failed! Retry.")
    else:
          if(command_status=="off"): # Conversation mode
              try:
                try:
                    os.remove("voice.wav")  # Remove file if already exists
                except:
                    print("File dont exist!")
                file_id = update.message.voice.file_id # get file id
                print("File id:"+file_id)
                newFile = bot.get_file(file_id)  # Get file name
                print("Voice message detected.")
                newFile.download('voice.ogg')   # Download the file
                os.system("ffmpeg -i voice.ogg voice.wav 2>null") # convert to wav
                with sr.AudioFile('voice.wav') as source:   # record audio for google
                     print("Converting audio file.")
                     audio =r.record(source)
                try:
                   message=r.recognize_google(audio)   # recognize voice to text
                   print("Recognized message is: "+message)
                   update.message.reply_text(str(message)) # feedback
                   flag=1 # Flag for message decoded
                except:
                   print("Google recognize failure!, fallback.")
                   flag=0 # message not decoded
              except:
                   message=update.message.text  # rollback to text mode
                   print(update.message.from_user.username+":"+update.message.text) # debug
                   flag=1 # message decoded
                   print("Text message detected.")
              if(flag==1):
                   print("about to respond");
                   print(update.message.from_user.username+":"+str(message))
                   userid=checkdb(bot, update)[1]
                   resp,tag=response(str(message),userid,True)
                   print("Tag is :",tag)
                   print("AI:"+str(resp))
              try:
                   message=str(resp).lower()
                   if("on" in message and "light" in message):
                        ON(b"13")
                   if("off" in message and "light" in message):
                        OFF(b"13")
                   if(tag.strip()=="greetings"):                   # Checking if tag is greetings
                       resp+=checkdb(bot, update)[0]            # Getting info from user database
                   update.message.reply_text(str(resp))   # Reply the text from jarvis
                   tts=gTTS(text=str(resp),lang="en")
                   tts.save("response")
                   print("saved")
                   bot.send_voice(chat_id=update.message.chat_id, voice=open('response', 'rb'))
              except:
                    print("Failed!")

def mainswitch(bot,update):
     message=update.message.text
     print("mainswitch :"+message)
     print(update.message.from_user.username+":"+update.message.text)
     value=message.strip("/all").strip()
     
     if value=="on":
          status=value
          value=1
     elif value=="off":
          status=value
          value=0
     else:
         status=''
     if status != '' :
         os.system("./main_switch.bash "+str(value))
         update.message.reply_text("Turned all lights "+status)
         
     else :
         update.message.reply_text("Unknown Command")
         
         
# Telegram Bot Definitions...
try: 
   key=open("conf.ini",'r').read().strip()
except: 
   print("Error occured, try running setup.py")
   exit()
updater = Updater(key)
updater.dispatcher.add_handler(MessageHandler(Filters.text,message))
updater.dispatcher.add_handler(MessageHandler(Filters.audio,message))
updater.dispatcher.add_handler(MessageHandler(Filters.voice,message))
updater.dispatcher.add_handler(CommandHandler('iot',iot))
updater.dispatcher.add_handler(CommandHandler('ioton',ioton))
updater.dispatcher.add_handler(CommandHandler('iotoff',iotoff))
updater.dispatcher.add_handler(CommandHandler('status',status))
updater.dispatcher.add_handler(CommandHandler('mode' ,mode))
updater.dispatcher.add_handler(CommandHandler("all",mainswitch))


updater.start_polling()
