import telnetlib
import time
PORT = 23
HOST = "192.168.43.1"
global tn
def reconnect():
    global tn
    tn=telnetlib.Telnet(HOST,PORT)
def ON(pin):
    try:
        tn.write(pin+b" 1\r\n")
        time.sleep(0.3)
        reply=(tn.read_until(b"\r\n",0.1)).decode("utf-8") 
        #print("ON ",reply)
        if(not reply=="13 on\r\n"):
            print("Error occured!")
            ON(pin)
    except:
        reconnect()
        time.sleep(1)
        ON(pin)
    time.sleep(0.5)
def OFF(pin):
    try:
        tn.write(pin+b" 0\r\n")
        time.sleep(0.3)
        reply=(tn.read_until(b"\r\n",0.1)).decode("utf-8") 
        #print("OFF ",reply)
        if(not str(reply)=="13 off\r\n"):
            print("Error occured!")
            OFF(pin)
    except:
        reconnect()
        time.sleep(1)
        OFF(pin)
    time.sleep(0.5)
def close():
    try:
        tn.close()
    except:
        print("Already closed!")
#while(1):
